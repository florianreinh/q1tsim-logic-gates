pub mod q1tlogic {
    extern crate q1tsim;
    use q1tsim::{circuit, gates};

    pub fn create_and(
        circuit: &mut circuit::Circuit,
        operand_qbits: Vec<usize>,
        result_qbit: usize,
    ) -> &circuit::Circuit {
        let mut target_qbits = operand_qbits.clone();
        target_qbits.push(result_qbit);

        circuit
            .add_gate(gates::CCX::new(), target_qbits.as_slice())
            .unwrap();

        circuit
    }

    pub fn create_or(
        circuit: &mut circuit::Circuit,
        operand_qbits: Vec<usize>,
        result_qbit: usize,
    ) -> &circuit::Circuit {
        let mut target_qbits = operand_qbits.clone();
        target_qbits.push(result_qbit);

        circuit.cx(target_qbits[0], result_qbit).unwrap();
        circuit.cx(target_qbits[1], result_qbit).unwrap();
        circuit
            .add_gate(gates::CCX::new(), target_qbits.as_slice())
            .unwrap();

        circuit
    }

    pub fn create_xor(
        circuit: &mut circuit::Circuit,
        operand_qbits: Vec<usize>,
    ) -> &circuit::Circuit {
        circuit.cx(operand_qbits[0], operand_qbits[1]).unwrap();
        circuit.x(operand_qbits[1]).unwrap();

        circuit
    }
}

#[cfg(test)]
mod tests {
    extern crate q1tsim;
    use crate::q1tlogic;
    use q1tsim::circuit;

    #[test]
    fn test_create_and() {
        let mut circuit = circuit::Circuit::new(3, 3);

        circuit.h(0).unwrap();
        circuit.h(1).unwrap();

        q1tlogic::create_and(&mut circuit, vec![0, 1], 2);

        circuit.measure_all(&[0, 1, 2]).unwrap();
        circuit.execute(8192).unwrap();

        let histo = circuit.histogram_string().unwrap();

        let mut results: Vec<_> = histo.keys().collect();
        results.sort();

        assert_eq!(vec!["000", "001", "010", "111"], results);
    }
    #[test]
    fn test_create_or() {
        let mut circuit = circuit::Circuit::new(3, 3);

        circuit.h(0).unwrap();
        circuit.h(1).unwrap();

        q1tlogic::create_or(&mut circuit, vec![0, 1], 2);

        circuit.measure_all(&[0, 1, 2]).unwrap();
        circuit.execute(8192).unwrap();

        let histo = circuit.histogram_string().unwrap();

        let mut results: Vec<_> = histo.keys().collect();
        results.sort();

        assert_eq!(vec!["000", "101", "110", "111"], results);
    }

    #[test]
    fn test_create_xor() {
        let mut circuit = circuit::Circuit::new(2, 2);
        circuit.h(0).unwrap();
        q1tlogic::create_xor(&mut circuit, vec![0, 1]);

        circuit.measure_all(&[0, 1]).unwrap();
        circuit.execute(8192).unwrap();

        let histo = circuit.histogram_string().unwrap();

        let mut results: Vec<_> = histo.keys().collect();
        results.sort();

        assert_eq!(vec!["01", "10"], results);
    }
}
